Candidates to be added
======================

## Up and coming smartphone apps (add after testing):

### Ready to be added 
* https://github.com/OpenMandrivaSoftware/om-phone (Open Mandriva Dialer app)
* https://github.com/OpenMandrivaSoftware/om-camera (Open Mandriva Camera app)

### Needs help testing
* rokugtk https://github.com/cliftonts/rokugtk (a Roku remote app, I don't have a roku device so can't test)

### Needs testing
* xournalpp https://github.com/xournalpp/xournalpp
* opensnitch https://github.com/evilsocket/opensnitch/discussions/415
* Moody https://gitlab.gnugen.ch/afontain/moodle
* Kalendar https://invent.kde.org/pim/kalendar
* PineCast https://github.com/Jeremiah-Roise/PineCast

### WIP (needs more work) and further testing
* PinePhone modem firmware updater UI: https://git.sr.ht/~martijnbraam/pinephone-modemfw (did not work yet for me, go to https://github.com/Biktorgj/quectel_eg25_recovery for now)
* https://invent.kde.org/carlschwan/quickmail (might now be "Kolibri", see https://www.plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/)
* https://invent.kde.org/vandenoever/mailmodel
* https://gitlab.gnome.org/bilelmoussaoui/camera-rs
* https://gitlab.gnome.org/bilelmoussaoui/paintable-rs
* https://gitlab.com/bhdouglass/rockwork (if it can be brought over?)
* https://github.com/naxuroqa/venom (A modern Tox client for the Linux desktop, not mobile friendly)
* https://github.com/nbdy/pui (launcher?)
* https://github.com/nahuelwexd/Replay (YouTube client)
* https://gitlab.gnome.org/GNOME/gnome-network-displays (Flathub: https://flathub.org/apps/details/org.gnome.NetworkDisplays; AUR: gnome-network-displays(-git)
* https://codeberg.org/xaviers/Interpret (GTK/Rust deepl based translation app, translation did not work in my testing)
* https://blogs.gnome.org/chergert/2021/03/06/a-gtk-4-based-text-editor/ 
* https://git.sr.ht/~link2xt/kdeltachat (deltachat client)
* https://github.com/JNissi/camcam WIP Camera app written in Rust
* https://github.com/JNissi/fastcal WIP Calendar app written in Rust
* https://github.com/DavidVentura/hn Python hacker news client
* https://invent.kde.org/maui/booth (camera)
* https://cubocore.org/ apps, see also https://wiki.postmarketos.org/wiki/CoreApps
* https://invent.kde.org/openwebdesigngermany/musik 
* https://invent.kde.org/devinlin/solio
* https://github.com/Cyborgscode/wifiscanner (according to the readme the GUI does not work on PinePhone, so ... Not sure.)
* https://gitlab.gnome.org/GabMus/ghostshot
* https://github.com/ripxorip/hydro_bot ("Pinephone app that keeps you hydrated", too much WIP to add)


### Needs initial testing
* Moody https://gitlab.gnugen.ch/afontain/moodle
* Retiled (Windows Phone style launcher) https://github.com/DrewNaylor/Retiled
* https://codeberg.org/xaviers/Comlink
* https://github.com/Cyborgscode/Personal-Voice-Assistent (Currently German only, requires Java. See also https://marius.bloggt-in-braunschweig.de/2021/07/15/hallo-computer-bist-du-da/)
* https://github.com/timaios/PineConnect (early state of development, work on the app has not started yet (but the daemon is being worked on))
* https://gitlab.com/Aresesi/mactrack 
* https://framagit.org/tytan652/https-customs
* https://codeberg.org/tpikonen/satellite
* https://gitlab.com/LyesSaadi/sharit
* https://github.com/artemanufrij/metronome


### Projects that seem to be abandoned
* https://github.com/scandinave/kodimote (WIP, No commits in 16 months, but..)
* https://source.puri.sm/fphemeral/l5_shoppinglist,https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopter
* https://source.puri.sm/fphemeral/librem5_utils,https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopter/
    
## old Linux Phone apps:
* Navit
* FoxtrotGPS
* Maep
    
## unoptimized desktop apps that have been run on the Librem 5 or PinePhone successfully (only add if they are at least somewhat usable):
* Gnome Photos
* Gthumb
* Shotwell // apps from here on where spotted on a screenshot
* Claws Mail
* D-Feet
* Gpredict
* KiCad
* Shadowsocks-Qt5
* [Lyrebird](https://github.com/constcharptr/lyrebird)
* https://github.com/linuxmint/webapp-manager
* Verbiste https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7
* Simple scan https://gitlab.gnome.org/GNOME/simple-scan (newer versions are only fine after "scale-to-fit simple-scan on")
* Marble (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/27)
* Orage (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/28)
* Solar System (https://flathub.org/apps/details/org.sugarlabs.SolarSystem; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/37)
* AusweisApp 2 (https://flathub.org/apps/details/de.bund.ausweisapp.ausweisapp2; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/51)
* Nixwriter https://flathub.org/apps/details/com.gitlab.adnan338.Nixwriter; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/69
* SongRec https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/91
* Bleachbit https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/101
* Argos Translate https://github.com/argosopentech/argos-translate

## Remains of other-apps.csv
Check, then sort or remove the following:

### This is a loose and non-formal collection of apps which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the other list"
* Roger Router,https://gitlab.com/tabos/rogerrouter,https://tabos.gitlab.io/project/rogerrouter/,,GNU GPLv2,,Roger Router is a utility to control and monitor AVM Fritz!Box
* MedOS-kirigami,https://github.com/milohr/medOS-kirigami
* clan https://github.com/mauikit/clan (Seems to have been a launcher)
* Liri Screencast,https://github.com/lirios/screencast
* Liri Screenshot,https://github.com/lirios/screenshot
* Liri music,https://github.com/lirios/music
* Liri Settings,https://github.com/lirios/settings
* Liri NetworkManager,https://github.com/lirios/networkmanager
* OwnCloud Sync,https://open-store.io/app/owncloud-sync
* Music,https://wiki.gnome.org/Apps/Music,https://tchncs.de/_matrix/media/v1/download/talk.puri.sm/wDbVJsNtmaLUljbuxVVzRRhf
* GadgetBridge https://github.com/Freeyourgadget/Gadgetbridge (afaik Android only, so: why?)
* Linphone (not mobile compatible, Ubuntu Touch version does not exist for 64bit yet)
* https://doc.qt.io/qt-5/qtquick-codesamples.html

### In planning stage:
* QR Code Scanner https://phabricator.kde.org/T8906
* Compass https://phabricator.kde.org/T8905
* Konversation 2.0
* Apps where the sources are not shared yet:
* Translator by Avi Wadhwa in community/librem-5-apps announced

### Sources for apps to be included:
* https://binary-factory.kde.org/view/Android/,28 apps (02.03.2020)
* https://www.youtube.com/channel/UCIVaIdjtr6aNPdTm-JNbFAg/videos
* https://www.youtube.com/watch?v=K4OfNFis--g
* https://puri.sm/posts/what-is-mobile-pureos/
* https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development
* https://nxos.org/maui/maui-apps-1-2-1-released-and-more/ (check whether everything is already on the list)
